package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import javax.xml.xpath.XPath;
import java.util.List;

public class SearchResult {
    WebDriver driver = null;

    public SearchResult(WebDriver driver)
    {
        PageFactory.initElements(driver, this);
        this.driver =driver;
    }

    @FindBy(xpath="//a//span[contains(text(),'czarny')]")
    private WebElement blackColor;

    @FindBy(xpath="//select[@class='_1h7wt _k70df _7qjq4 _27496_3VqWr']//option[@value='pd']")
    private WebElement sortingElementByHighest;

    @FindBy(xpath="(//span[@class='_9c44d_1zemI'])[3]")
    private WebElement highestPrice;

    @FindBy(xpath = "//div[@class='opbox-listing--base']//h2/a[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')")
    private WebElement resultItemOfSearch;

    @FindBy(css = "body > div.main-wrapper > div:nth-child(4) > div > div > div > div > div > div._1yyhi._e219d_2fgnH > div._3kk7b._n1rmb._1t6t8._e219d_1YFA5._e219d_2A8fy > div:nth-child(1) > div:nth-child(1) > div > div > div > section > div:nth-child(3) > div > div > ul:nth-child(2) > li > ul > li > a")
    private WebElement applecategory;


    public  void selectApplecategory(){
        applecategory.click();
    }
    public void selectBlackColor()
    {
    blackColor.click();
    }

    public void setSortingElementByHighest(){
        sortingElementByHighest.click();
    }

    public int checkHowManyProductsOnResult(){
        List<WebElement> products = driver.findElements(By.xpath("//div[@class='opbox-listing--base']//h2/a[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'),'iphone 11')]"));
        return products.size();
    }

    public double highestPriceOfProduct(){
        String firstProductPriceString = highestPrice.getText();
        String cleaned_price = firstProductPriceString.replace(" ", "").replace("zł", "").replace(",",".");
        return Double.parseDouble(cleaned_price);
    }
}
