package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import javax.naming.Name;

public class HomePage {
WebDriver driver = null;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//button[@data-role='accept-consent']")
    private WebElement acceptCookiesButton;

    @FindBy(name="string")
    private WebElement inputSearch;

    public  void setAcceptCookiesButton(){
        acceptCookiesButton.click();
    }

    public void sentTextToInputSearch(String searchText){
        inputSearch.sendKeys(searchText);
        inputSearch.sendKeys(Keys.ENTER);
    }
}
