package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.SearchResult;

import java.util.concurrent.TimeUnit;

public class AllegroTest {

    WebDriver driver = null;

    @BeforeTest
    public void setUpTest()
    {
        String projectPath = System.getProperty("user.dir");
        System.setProperty("webdriver.chrome.driver",  projectPath+"//drivers//chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get("https://allegro.pl/");
    }

    @Test
    public void allegroSearch(){
        HomePage homePage = new HomePage(driver);
        SearchResult searchResult = new SearchResult(driver);
        homePage.setAcceptCookiesButton();
        homePage.sentTextToInputSearch("Iphone 11");

        searchResult.selectApplecategory();
        searchResult.selectBlackColor();
        searchResult.setSortingElementByHighest();

        // how many iphone 11
        int numOfProduct = searchResult.checkHowManyProductsOnResult();
        System.out.println("found "+numOfProduct+" Iphone 11");

        // most expensive iphone
        double mostExpensiveIphone = searchResult.highestPriceOfProduct();
        System.out.println("the most expensive Iphone 11 costs "+mostExpensiveIphone + " złotych");

        // price the most expensive iphone plus 23%
        double mostExpensiveIphonePlus23proc = (mostExpensiveIphone * 1.23);
        System.out.println("the most expensive Iphone 11 costs plus 23 procent cost " + mostExpensiveIphonePlus23proc);
    }

    @AfterTest
    public void afterTest()
    {
        driver.close();
        driver.quit();
    }
   }




